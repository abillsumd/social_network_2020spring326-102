""" A simple social network. """

class Person:
    """ A person in a social network.
    
    Attributes:
        name (str): the person's name
        connections (set of Person): the person's connections
    """
    def __init__(self, name):
        self.name = name
        self.connections = set()
    
    def add_connection(self, other):
        """ Add other as a connection.
        
        Args:
            other (Person): the person we want to connect with.
        """
        if other not in self.connections:
            self.connections.add(other)
            other.add_connection(self)
    
    def get_second_connections(self):
        """ Return a set of the person's second connections, not
        including any of their first connections or themselves.
        
        Returns:
            set of Person: the second connections.
        """
        sc = set()
        for x in self.connections:
            sc |= x.connections
        sc -= self.connections
        sc -= {self}
        return sc
        